<?php

#server key of firebase app
define('FIREBASE_API_KEY', 'AIzaSyABkjOkXZRtW5vlV99XMAWUEQqnySeCMAk');

class Firebase {
    // sending push message to single user by firebase reg id

    public function sendNotification($reg_id_s, $msg) {
        $pushMsg['notification']=$msg;
        //$pushMsg['registration_ids']=$reg_id_s;

        if(is_array($reg_id_s)){
            $pushMsg['registration_ids'] = $reg_id_s;
        }else{
            $pushMsg['to'] = $reg_id_s;
        }
        return $this->sendPushNotification($pushMsg);
    }

    public function sendData($reg_id_s, $msg) {
        $pushMsg['data']=$msg;
        $pushMsg['to']=$reg_id_s;
        return $this->sendPushNotification($pushMsg);
    }


    // Sending message to a topic by topic name
    public function sendNotificationToTopic($topic, $msg) {
        $pushMsg['notification']=$msg;
        $pushMsg['to']= '/topics/' . $topic;
        return $this->sendPushNotification($pushMsg);
    }

    // Sending message to a topic by topic name
    public function sendDataToTopic($topic, $msg) {
        $pushMsg['data']=$msg;
        $pushMsg['to']= '/topics/' . $topic;
        return $this->sendPushNotification($pushMsg);
    }

    // function makes curl request to firebase servers
    private function sendPushNotification($pushMsg) {
        // Set POST variables
        $url = 'https://fcm.googleapis.com/fcm/send';

        $headers = array('Authorization: key=' . FIREBASE_API_KEY,'Content-Type: application/json');
        // Open connection
        //some problem here - resolved by installing: sudo apt-get install php7.0-curl 
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($pushMsg));

        // Execute post
        $result = curl_exec($ch);
        if ($result === FALSE) {
            echo "curl : error sending notification\n";
            die('Curl failed: ' . curl_error($ch));
        }

        echo "sent notification - got fcm response:\n";
        echo json_encode($result);
        // Close connection
        curl_close($ch);
        return $result;
    }

    // function makes curl request to firebase servers
    private function sendPushNotificationUsingContextStream($pushMsg) {
        // Set POST variables
        $url = 'https://fcm.googleapis.com/fcm/send';

        $headers = array('Authorization: key=' . FIREBASE_API_KEY,'Content-Type: application/json');
        $data = json_encode($pushMsg);
        // $data = http_build_query($data);

        $context_options = array (
            'http' => array (
                'method' => 'POST',
                'header'=>  "Content-type: application/json\r\n"
                . 'Authorization: key=' . FIREBASE_API_KEY. "\r\n"
                . "Content-Length: " . strlen($data) . "\r\n",
                'content' => $data
                )
            );

        $context = stream_context_create($context_options);
        $result=file_get_contents($url,false,$context);
        //$fp = fopen($url, 'r', false, $context);
    }
}

?>