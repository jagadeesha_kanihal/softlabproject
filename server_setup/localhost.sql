-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 20, 2016 at 11:24 PM
-- Server version: 5.7.16-0ubuntu0.16.04.1
-- PHP Version: 7.0.8-0ubuntu0.16.04.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eventDB`
--
CREATE DATABASE IF NOT EXISTS `eventDB` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `eventDB`;

-- --------------------------------------------------------

--
-- Table structure for table `Days`
--

CREATE TABLE `Days` (
  `eDay` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Days`
--

INSERT INTO `Days` (`eDay`) VALUES
('Friday'),
('Monday'),
('Saturday'),
('Sunday'),
('Thursday'),
('Tuesday'),
('Wednesday');

-- --------------------------------------------------------

--
-- Table structure for table `credentialsTable`
--

CREATE TABLE `credentialsTable` (
  `userID` int(6) NOT NULL,
  `cseMail` varchar(100) NOT NULL,
  `password` varchar(128) NOT NULL,
  `iitbMail` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `credentialsTable`
--

INSERT INTO `credentialsTable` (`userID`, `cseMail`, `password`, `iitbMail`) VALUES
(1, 'dummy@cse.iitb.ac.in', 'dummy', 'dummy@iitb.ac.in'),
(2, 'sample@cse.iitb.ac.in', 'sample', 'sample.iitb.ac.in'),
(3, 'test@cse.iitb.ac.in', 'test', NULL),
(4, 'admin@cse.iitb.ac.in', 'admin', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `groupMeta`
--

CREATE TABLE `groupMeta` (
  `groupID` varchar(256) NOT NULL,
  `groupMeta` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `groupMeta`
--

INSERT INTO `groupMeta` (`groupID`, `groupMeta`) VALUES
('CS601-Algo', 'Prof.Sundar V'),
('CS699-Software Lab', 'Prof. RK Joshi'),
('Mtech1', 'All Mtech 1 Students');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `groupID` varchar(256) NOT NULL,
  `userID` int(11) NOT NULL,
  `isAdmin` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`groupID`, `userID`, `isAdmin`) VALUES
('CS601-Algo', 1, 1),
('CS601-Algo', 2, NULL),
('CS699-Software Lab', 1, 1),
('CS699-Software Lab', 2, NULL),
('Mtech1', 1, 1),
('Mtech1', 2, 1),
('Mtech1', 3, NULL),
('Mtech1', 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `nonRecurringEvents`
--

CREATE TABLE `nonRecurringEvents` (
  `nrEventID` int(11) NOT NULL,
  `eDate` date NOT NULL,
  `startTime` time NOT NULL,
  `endTime` time NOT NULL,
  `eventName` varchar(256) NOT NULL,
  `eventPerson` varchar(128) DEFAULT NULL,
  `eventDescription` text,
  `groupID` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nonRecurringEvents`
--

INSERT INTO `nonRecurringEvents` (`nrEventID`, `eDate`, `startTime`, `endTime`, `eventName`, `eventPerson`, `eventDescription`, `groupID`) VALUES
(1, '2016-11-09', '17:30:00', '18:30:00', 'FUSS Talk', 'Prof.XYZ', 'Talk is about so and so topic.', 'Mtech1'),
(2, '2016-11-02', '17:30:00', '18:30:00', 'FUSS Talk', 'Prof.ABC', 'Talk is about so and so topic.', 'CS699-Software Lab'),
(3, '2016-10-30', '13:40:00', '23:59:00', 'FUSS Talk', 'Prof.ABC', 'Talk is about so and so topic @ so and so place.', 'CS699-Software Lab'),
(4, '2016-10-30', '10:55:00', '23:59:00', 'FUSS Talk', 'Prof.ABC', 'Talk is about so and so topic @ so and so place.', 'CS699-Software Lab'),
(5, '2016-10-31', '11:32:00', '23:59:00', 'FUSS Talk', 'Prof.ABC', 'Talk is about so and so topic @ so and so place.', 'CS699-Software Lab'),
(6, '2016-10-31', '16:37:00', '23:59:00', 'FUSS Talk', 'Prof.ABC', 'Talk is about so and so topic @ so and so place.', 'CS699-Software Lab'),
(7, '2016-10-31', '17:38:00', '23:59:00', 'FUSS Talk', 'Prof.ABC', 'Talk is about so and so topic @ so and so place.', 'CS699-Software Lab'),
(8, '2016-10-31', '16:35:41', '23:59:00', 'FUSS Talk', 'Prof.ABC', 'Talk is about so and so topic @ so and so place.', 'CS699-Software Lab'),
(9, '2016-10-31', '16:34:41', '23:59:00', 'FUSS Talk', 'Prof.ABC', 'Talk is about so and so topic @ so and so place.', 'CS699-Software Lab'),
(10, '2016-11-09', '19:50:41', '14:20:00', 'FUSS Talk', 'Prof.ABC', '7:50 PM non rec event 9th nov', 'CS699-Software Lab'),
(11, '2016-11-09', '17:30:00', '18:30:00', 'FUSS Talk', 'Prof.XYZ', 'Talk is about so and so topic.', 'Mtech1'),
(12, '2016-11-09', '17:30:00', '18:30:00', 'Talk', 'Prof.XYZ', 'Talk is about so and so topic.', 'Mtech1'),
(13, '2016-10-22', '16:00:00', '17:51:00', 'cs699demo', 'prashanth', 'soft lab project', 'CS601-Algo'),
(14, '2016-10-08', '16:53:00', '17:53:00', 'softlab', 'prashanth', 'demo', 'CS601-Algo'),
(15, '2016-11-08', '18:00:00', '19:00:00', 'soft lab dem', 'Jagadeesha', 'soft lab dem\nJagadeesha', 'CS601-Algo'),
(16, '2016-11-09', '23:35:00', '23:40:00', 'softdemo', 'Prashanth', 'softdemo\nPrashanth', 'Mtech1'),
(17, '2016-11-09', '23:38:00', '23:50:00', 'gdhd', 'shvd', 'gabc', 'Mtech1');

-- --------------------------------------------------------

--
-- Table structure for table `recurringEvents`
--

CREATE TABLE `recurringEvents` (
  `rEventID` int(11) NOT NULL,
  `eDay` varchar(30) NOT NULL,
  `startTime` time NOT NULL,
  `endTime` time NOT NULL,
  `slotNo` varchar(30) DEFAULT NULL,
  `eventName` varchar(256) NOT NULL,
  `eventPerson` varchar(128) DEFAULT NULL,
  `eventDescription` text,
  `groupID` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recurringEvents`
--

INSERT INTO `recurringEvents` (`rEventID`, `eDay`, `startTime`, `endTime`, `slotNo`, `eventName`, `eventPerson`, `eventDescription`, `groupID`) VALUES
(1, 'Monday', '19:02:00', '12:30:00', '4A', 'CS699-Lab Lecture', 'Prof.RKJ', '', 'CS699-Software Lab'),
(2, 'Wednesday', '19:00:03', '17:00:00', 'LX', 'CS699-Lab', 'Prof.RKJ', 'Software Lab ', 'CS699-Software Lab'),
(3, 'Tuesday', '10:30:00', '11:30:00', '4A', 'CS601-Lecture', 'Prof.SV', NULL, 'CS601-Algo'),
(4, 'Friday', '11:30:00', '12:30:00', '4A', 'CS699-Lab Lecture', 'Prof.RKJ', NULL, 'CS699-Software Lab'),
(5, 'Thursday', '13:30:00', '14:30:00', '4b', 'CS601-Lecture', 'Prof.SV', 'CS601-Lecture\nProf.SV\n4A', 'CS601-Algo'),
(6, 'Saturday', '14:00:00', '17:00:00', 'LX', 'CS699-Lab', 'Prof.RKJ', NULL, 'CS699-Software Lab'),
(7, 'Sunday', '10:30:00', '11:30:00', '4A', 'CS601-Lecture', 'Prof.SV', NULL, 'CS601-Algo'),
(8, 'Sunday', '08:38:00', '14:30:00', '4A', 'CS601-Lecture', 'Prof.SV', 'CS601-Lecture\nProf.SV\n4A', 'CS601-Algo'),
(9, 'Monday', '10:35:00', '12:13:00', '4A', 'CS699-Lab Lecture', 'Prof.RKJ', '', 'CS699-Software Lab'),
(10, 'Monday', '15:35:00', '16:59:00', '4A', 'CS601-Lecture', 'Prof.SV', 'CS601-Lecture\nProf.SV\n4A', 'Mtech1'),
(11, 'Monday', '20:54:00', '12:13:00', '4A', 'CS699-Lab Lecture', 'Prof.RKJ', '', 'CS699-Software Lab'),
(12, 'Thursday', '10:30:00', '11:30:00', '4A', 'CS601-Lecture', 'Prof.SVhdhs', 'CS601-Lecture\nProf.SV\n4A', 'CS601-Algo'),
(13, 'Monday', '15:02:00', '12:30:00', '4A', 'CS699-Lab Lecture', 'Prof.RKJ', '', 'Mtech1'),
(14, 'Sunday', '08:38:00', '17:30:00', '4A', 'CS601-Lecture', 'Prof.SV', 'Update test CS601-Lecture\nProf.SV\n4A', 'CS601-Algo'),
(15, 'Wednesday', '10:30:00', '11:30:00', '4A', 'CS601-Lecture', 'Prof.SV', '', 'CS601-Algo'),
(16, 'Wednesday', '19:47:03', '21:01:00', 'LX', 'CS699-Lab', 'Prof.RKJ', '7:47PM notification- Wednesday', 'CS699-Software Lab');

-- --------------------------------------------------------

--
-- Table structure for table `regIdTable`
--

CREATE TABLE `regIdTable` (
  `userID` int(11) NOT NULL,
  `regID` varchar(256) NOT NULL,
  `deviceID` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `regIdTable`
--

INSERT INTO `regIdTable` (`userID`, `regID`, `deviceID`) VALUES
(1, 'dbV5P2fnPVI:APA91bE3i55scxqhfJem6vYO9Gac0WQgGRLUwNlQ2t2h8x4VQYLC7uXJAraF5_RBq_5akGP4IYJc_g8Jo-imXeKpuGJM43O9aGvB1Jd0C064pKmH3PfwnRByyy7vJSnNrApql3855XGY', NULL),
(2, 'cYCZq2RPMZo:APA91bErVDhIjiN9E8chEr6ZnW7TmsV_Ozxltkm2ILatyOOxwhrQazl3J_7TCuLZOD5k7ssUgyLJheOqfCg5CEApxRkHJEorzpwAymOhQXfJ7-ZtqlZoT1AIKv9gN31EXNPpHGbTGWFt', NULL),
(3, 'jkl12345', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Days`
--
ALTER TABLE `Days`
  ADD PRIMARY KEY (`eDay`);

--
-- Indexes for table `credentialsTable`
--
ALTER TABLE `credentialsTable`
  ADD PRIMARY KEY (`userID`),
  ADD UNIQUE KEY `userID` (`userID`),
  ADD UNIQUE KEY `cseMail` (`cseMail`);

--
-- Indexes for table `groupMeta`
--
ALTER TABLE `groupMeta`
  ADD PRIMARY KEY (`groupID`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`groupID`,`userID`);

--
-- Indexes for table `nonRecurringEvents`
--
ALTER TABLE `nonRecurringEvents`
  ADD PRIMARY KEY (`nrEventID`);

--
-- Indexes for table `recurringEvents`
--
ALTER TABLE `recurringEvents`
  ADD PRIMARY KEY (`rEventID`),
  ADD UNIQUE KEY `eventID` (`rEventID`);

--
-- Indexes for table `regIdTable`
--
ALTER TABLE `regIdTable`
  ADD PRIMARY KEY (`userID`,`regID`),
  ADD KEY `userID` (`userID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `credentialsTable`
--
ALTER TABLE `credentialsTable`
  MODIFY `userID` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `nonRecurringEvents`
--
ALTER TABLE `nonRecurringEvents`
  MODIFY `nrEventID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `recurringEvents`
--
ALTER TABLE `recurringEvents`
  MODIFY `rEventID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;--
-- Database: `phpmyadmin`
--
CREATE DATABASE IF NOT EXISTS `phpmyadmin` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `phpmyadmin`;

-- --------------------------------------------------------

--
-- Table structure for table `pma__bookmark`
--

CREATE TABLE `pma__bookmark` (
  `id` int(11) NOT NULL,
  `dbase` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `label` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `query` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Bookmarks';

-- --------------------------------------------------------

--
-- Table structure for table `pma__central_columns`
--

CREATE TABLE `pma__central_columns` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_type` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_length` text COLLATE utf8_bin,
  `col_collation` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_isNull` tinyint(1) NOT NULL,
  `col_extra` varchar(255) COLLATE utf8_bin DEFAULT '',
  `col_default` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Central list of columns';

-- --------------------------------------------------------

--
-- Table structure for table `pma__column_info`
--

CREATE TABLE `pma__column_info` (
  `id` int(5) UNSIGNED NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `column_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `comment` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `mimetype` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `transformation` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `transformation_options` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `input_transformation` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `input_transformation_options` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Column information for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__designer_settings`
--

CREATE TABLE `pma__designer_settings` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `settings_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Settings related to Designer';

-- --------------------------------------------------------

--
-- Table structure for table `pma__export_templates`
--

CREATE TABLE `pma__export_templates` (
  `id` int(5) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `export_type` varchar(10) COLLATE utf8_bin NOT NULL,
  `template_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `template_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Saved export templates';

-- --------------------------------------------------------

--
-- Table structure for table `pma__favorite`
--

CREATE TABLE `pma__favorite` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `tables` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Favorite tables';

-- --------------------------------------------------------

--
-- Table structure for table `pma__history`
--

CREATE TABLE `pma__history` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `timevalue` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sqlquery` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='SQL history for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__navigationhiding`
--

CREATE TABLE `pma__navigationhiding` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `item_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `item_type` varchar(64) COLLATE utf8_bin NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Hidden items of navigation tree';

-- --------------------------------------------------------

--
-- Table structure for table `pma__pdf_pages`
--

CREATE TABLE `pma__pdf_pages` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `page_nr` int(10) UNSIGNED NOT NULL,
  `page_descr` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='PDF relation pages for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__recent`
--

CREATE TABLE `pma__recent` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `tables` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Recently accessed tables';

--
-- Dumping data for table `pma__recent`
--

INSERT INTO `pma__recent` (`username`, `tables`) VALUES
('root', '[{"db":"eventDB","table":"nonRecurringEvents"},{"db":"eventDB","table":"recurringEvents"},{"db":"eventDB","table":"groups"},{"db":"eventDB","table":"regIdTable"},{"db":"eventDB","table":"credentialsTable"},{"db":"eventDB","table":"groupMeta"},{"db":"eventDB","table":"Days"},{"db":"phpmyadmin","table":"pma__bookmark"}]');

-- --------------------------------------------------------

--
-- Table structure for table `pma__relation`
--

CREATE TABLE `pma__relation` (
  `master_db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `master_table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `master_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Relation table';

--
-- Dumping data for table `pma__relation`
--

INSERT INTO `pma__relation` (`master_db`, `master_table`, `master_field`, `foreign_db`, `foreign_table`, `foreign_field`) VALUES
('eventDB', 'groups', 'groupID', 'eventDB', 'groupMeta', 'groupID'),
('eventDB', 'groups', 'userID', 'eventDB', 'credentialsTable', 'userID'),
('eventDB', 'nonRecurringEvents', 'groupID', 'eventDB', 'groups', 'groupID'),
('eventDB', 'recurringEvents', 'eDay', 'eventDB', 'Days', 'eDay'),
('eventDB', 'recurringEvents', 'groupID', 'eventDB', 'groups', 'groupID'),
('eventDB', 'regIdTable', 'userID', 'eventDB', 'credentialsTable', 'userID');

-- --------------------------------------------------------

--
-- Table structure for table `pma__savedsearches`
--

CREATE TABLE `pma__savedsearches` (
  `id` int(5) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `search_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `search_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Saved searches';

-- --------------------------------------------------------

--
-- Table structure for table `pma__table_coords`
--

CREATE TABLE `pma__table_coords` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `pdf_page_number` int(11) NOT NULL DEFAULT '0',
  `x` float UNSIGNED NOT NULL DEFAULT '0',
  `y` float UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table coordinates for phpMyAdmin PDF output';

-- --------------------------------------------------------

--
-- Table structure for table `pma__table_info`
--

CREATE TABLE `pma__table_info` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `display_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table information for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__table_uiprefs`
--

CREATE TABLE `pma__table_uiprefs` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `prefs` text COLLATE utf8_bin NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Tables'' UI preferences';

-- --------------------------------------------------------

--
-- Table structure for table `pma__tracking`
--

CREATE TABLE `pma__tracking` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `version` int(10) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `schema_snapshot` text COLLATE utf8_bin NOT NULL,
  `schema_sql` text COLLATE utf8_bin,
  `data_sql` longtext COLLATE utf8_bin,
  `tracking` set('UPDATE','REPLACE','INSERT','DELETE','TRUNCATE','CREATE DATABASE','ALTER DATABASE','DROP DATABASE','CREATE TABLE','ALTER TABLE','RENAME TABLE','DROP TABLE','CREATE INDEX','DROP INDEX','CREATE VIEW','ALTER VIEW','DROP VIEW') COLLATE utf8_bin DEFAULT NULL,
  `tracking_active` int(1) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Database changes tracking for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__userconfig`
--

CREATE TABLE `pma__userconfig` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `timevalue` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `config_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='User preferences storage for phpMyAdmin';

--
-- Dumping data for table `pma__userconfig`
--

INSERT INTO `pma__userconfig` (`username`, `timevalue`, `config_data`) VALUES
('root', '2016-10-15 06:52:51', '{"collation_connection":"utf8mb4_unicode_ci"}');

-- --------------------------------------------------------

--
-- Table structure for table `pma__usergroups`
--

CREATE TABLE `pma__usergroups` (
  `usergroup` varchar(64) COLLATE utf8_bin NOT NULL,
  `tab` varchar(64) COLLATE utf8_bin NOT NULL,
  `allowed` enum('Y','N') COLLATE utf8_bin NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='User groups with configured menu items';

-- --------------------------------------------------------

--
-- Table structure for table `pma__users`
--

CREATE TABLE `pma__users` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `usergroup` varchar(64) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Users and their assignments to user groups';

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pma__bookmark`
--
ALTER TABLE `pma__bookmark`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pma__central_columns`
--
ALTER TABLE `pma__central_columns`
  ADD PRIMARY KEY (`db_name`,`col_name`);

--
-- Indexes for table `pma__column_info`
--
ALTER TABLE `pma__column_info`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `db_name` (`db_name`,`table_name`,`column_name`);

--
-- Indexes for table `pma__designer_settings`
--
ALTER TABLE `pma__designer_settings`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `pma__export_templates`
--
ALTER TABLE `pma__export_templates`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_user_type_template` (`username`,`export_type`,`template_name`);

--
-- Indexes for table `pma__favorite`
--
ALTER TABLE `pma__favorite`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `pma__history`
--
ALTER TABLE `pma__history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `username` (`username`,`db`,`table`,`timevalue`);

--
-- Indexes for table `pma__navigationhiding`
--
ALTER TABLE `pma__navigationhiding`
  ADD PRIMARY KEY (`username`,`item_name`,`item_type`,`db_name`,`table_name`);

--
-- Indexes for table `pma__pdf_pages`
--
ALTER TABLE `pma__pdf_pages`
  ADD PRIMARY KEY (`page_nr`),
  ADD KEY `db_name` (`db_name`);

--
-- Indexes for table `pma__recent`
--
ALTER TABLE `pma__recent`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `pma__relation`
--
ALTER TABLE `pma__relation`
  ADD PRIMARY KEY (`master_db`,`master_table`,`master_field`),
  ADD KEY `foreign_field` (`foreign_db`,`foreign_table`);

--
-- Indexes for table `pma__savedsearches`
--
ALTER TABLE `pma__savedsearches`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_savedsearches_username_dbname` (`username`,`db_name`,`search_name`);

--
-- Indexes for table `pma__table_coords`
--
ALTER TABLE `pma__table_coords`
  ADD PRIMARY KEY (`db_name`,`table_name`,`pdf_page_number`);

--
-- Indexes for table `pma__table_info`
--
ALTER TABLE `pma__table_info`
  ADD PRIMARY KEY (`db_name`,`table_name`);

--
-- Indexes for table `pma__table_uiprefs`
--
ALTER TABLE `pma__table_uiprefs`
  ADD PRIMARY KEY (`username`,`db_name`,`table_name`);

--
-- Indexes for table `pma__tracking`
--
ALTER TABLE `pma__tracking`
  ADD PRIMARY KEY (`db_name`,`table_name`,`version`);

--
-- Indexes for table `pma__userconfig`
--
ALTER TABLE `pma__userconfig`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `pma__usergroups`
--
ALTER TABLE `pma__usergroups`
  ADD PRIMARY KEY (`usergroup`,`tab`,`allowed`);

--
-- Indexes for table `pma__users`
--
ALTER TABLE `pma__users`
  ADD PRIMARY KEY (`username`,`usergroup`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pma__bookmark`
--
ALTER TABLE `pma__bookmark`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pma__column_info`
--
ALTER TABLE `pma__column_info`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pma__export_templates`
--
ALTER TABLE `pma__export_templates`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pma__history`
--
ALTER TABLE `pma__history`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pma__pdf_pages`
--
ALTER TABLE `pma__pdf_pages`
  MODIFY `page_nr` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pma__savedsearches`
--
ALTER TABLE `pma__savedsearches`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
