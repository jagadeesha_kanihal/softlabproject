#!/bin/bash
#LINUX AMP installation : use '-y' option for non interactive yes to all apt y/n prompts
#http://stackoverflow.com/questions/7739645/install-mysql-on-ubuntu-without-password-prompt
sudo apt-get update
sudo apt-get  install apache2
sudo apt-get  install mysql-server
sudo apt-get  install php7.0
sudo apt-get  install libapache2-mod-php7.0
sudo apt-get  install php7.0-mysql
sudo apt-get install php7.0-curl
sudo apt-get  install phpmyadmin 
sudo chmod 777 -R /var/www/;
sudo printf "<?php\nphpinfo();\n?>" > /var/www/html/info.php;
sudo systemctl restart apache2
#sudo service apache restart
