-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 21, 2016 at 01:58 AM
-- Server version: 5.7.16-0ubuntu0.16.04.1
-- PHP Version: 7.0.8-0ubuntu0.16.04.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eventDB`
--

-- --------------------------------------------------------

--
-- Table structure for table `credentialsTable`
--

CREATE TABLE `credentialsTable` (
  `userID` int(6) NOT NULL,
  `cseMail` varchar(100) NOT NULL,
  `password` varchar(128) NOT NULL,
  `iitbMail` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `credentialsTable`
--

INSERT INTO `credentialsTable` (`userID`, `cseMail`, `password`, `iitbMail`) VALUES
(1, 'student@cse.iitb.ac.in', 'student', 'student@iitb.ac.in'),
(2, 'prof@cse.iitb.ac.in', 'prof', 'prof@iitb.ac.in'),
(3, 'admin@cse.iitb.ac.in', 'admin', 'admin@iitb.ac.in');

-- --------------------------------------------------------

--
-- Table structure for table `Days`
--

CREATE TABLE `Days` (
  `eDay` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Days`
--

INSERT INTO `Days` (`eDay`) VALUES
('Friday'),
('Monday'),
('Saturday'),
('Sunday'),
('Thursday'),
('Tuesday'),
('Wednesday');

-- --------------------------------------------------------

--
-- Table structure for table `groupMeta`
--

CREATE TABLE `groupMeta` (
  `groupID` varchar(256) NOT NULL,
  `groupMeta` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `groupMeta`
--

INSERT INTO `groupMeta` (`groupID`, `groupMeta`) VALUES
('CS601-Algo', 'Prof.Sundar V'),
('CS699-Software Lab', 'Prof. RK Joshi'),
('Mtech1', 'All Mtech 1 Students');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `groupID` varchar(256) NOT NULL,
  `userID` int(11) NOT NULL,
  `isAdmin` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`groupID`, `userID`, `isAdmin`) VALUES
('CS601-Algo', 1, NULL),
('CS601-Algo', 2, 1),
('CS699-Software Lab', 1, NULL),
('CS699-Software Lab', 2, 1),
('Mtech1', 1, NULL),
('Mtech1', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `nonRecurringEvents`
--

CREATE TABLE `nonRecurringEvents` (
  `nrEventID` int(11) NOT NULL,
  `eDate` date NOT NULL,
  `startTime` time NOT NULL,
  `endTime` time NOT NULL,
  `eventName` varchar(256) NOT NULL,
  `eventPerson` varchar(128) DEFAULT NULL,
  `eventDescription` text,
  `groupID` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nonRecurringEvents`
--

INSERT INTO `nonRecurringEvents` (`nrEventID`, `eDate`, `startTime`, `endTime`, `eventName`, `eventPerson`, `eventDescription`, `groupID`) VALUES
(1, '2016-11-21', '02:00:00', '04:00:00', 'FUSS Talk', 'Prof.XYZ', 'Talk is about so and so topic @ fck.\r\n21 Nov, 2am', 'Mtech1'),
(2, '2016-11-21', '10:00:00', '13:00:00', 'FUSS Talk', 'Prof.ABC', 'FUSS Talk\nProf.ABC', 'CS699-Software Lab'),
(3, '2016-11-21', '17:00:00', '19:00:00', 'FUSS Talk', 'Prof.ABC', 'Talk is about so and so topic @ so and so place.\r\n21 Nov, 5pm', 'Mtech1');

-- --------------------------------------------------------

--
-- Table structure for table `recurringEvents`
--

CREATE TABLE `recurringEvents` (
  `rEventID` int(11) NOT NULL,
  `eDay` varchar(30) NOT NULL,
  `startTime` time NOT NULL,
  `endTime` time NOT NULL,
  `slotNo` varchar(30) DEFAULT NULL,
  `eventName` varchar(256) NOT NULL,
  `eventPerson` varchar(128) DEFAULT NULL,
  `eventDescription` text,
  `groupID` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recurringEvents`
--

INSERT INTO `recurringEvents` (`rEventID`, `eDay`, `startTime`, `endTime`, `slotNo`, `eventName`, `eventPerson`, `eventDescription`, `groupID`) VALUES
(18, 'Monday', '02:15:00', '07:10:00', '3', 'Algorithm class', 'Prof.SV', 'algo class, monday : 2.15 am @ FC Kohli auditorium', 'CS601-Algo'),
(19, 'Wednesday', '09:00:00', '11:00:00', '3', 'software lab', 'prof. RK Joshi', 'soft lab, wednesday : 2pm@ cs101 lab', 'CS699-Software Lab'),
(20, 'Thursday', '11:00:00', '12:00:00', '4A', 'Faculty Meet', 'Prof.Nutan L', 'faculty meet, thursday : 11am @ FC Kohli Audi', 'Mtech1');

-- --------------------------------------------------------

--
-- Table structure for table `regIdTable`
--

CREATE TABLE `regIdTable` (
  `userID` int(11) NOT NULL,
  `regID` varchar(256) NOT NULL,
  `deviceID` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `regIdTable`
--

INSERT INTO `regIdTable` (`userID`, `regID`, `deviceID`) VALUES
(1, 'dbV5P2fnPVI:APA91bE3i55scxqhfJem6vYO9Gac0WQgGRLUwNlQ2t2h8x4VQYLC7uXJAraF5_RBq_5akGP4IYJc_g8Jo-imXeKpuGJM43O9aGvB1Jd0C064pKmH3PfwnRByyy7vJSnNrApql3855XGY', NULL),
(2, 'dbV5P2fnPVI:APA91bE3i55scxqhfJem6vYO9Gac0WQgGRLUwNlQ2t2h8x4VQYLC7uXJAraF5_RBq_5akGP4IYJc_g8Jo-imXeKpuGJM43O9aGvB1Jd0C064pKmH3PfwnRByyy7vJSnNrApql3855XGY', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `credentialsTable`
--
ALTER TABLE `credentialsTable`
  ADD PRIMARY KEY (`userID`),
  ADD UNIQUE KEY `userID` (`userID`),
  ADD UNIQUE KEY `cseMail` (`cseMail`);

--
-- Indexes for table `Days`
--
ALTER TABLE `Days`
  ADD PRIMARY KEY (`eDay`);

--
-- Indexes for table `groupMeta`
--
ALTER TABLE `groupMeta`
  ADD PRIMARY KEY (`groupID`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`groupID`,`userID`);

--
-- Indexes for table `nonRecurringEvents`
--
ALTER TABLE `nonRecurringEvents`
  ADD PRIMARY KEY (`nrEventID`);

--
-- Indexes for table `recurringEvents`
--
ALTER TABLE `recurringEvents`
  ADD PRIMARY KEY (`rEventID`),
  ADD UNIQUE KEY `eventID` (`rEventID`);

--
-- Indexes for table `regIdTable`
--
ALTER TABLE `regIdTable`
  ADD PRIMARY KEY (`userID`,`regID`),
  ADD KEY `userID` (`userID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `credentialsTable`
--
ALTER TABLE `credentialsTable`
  MODIFY `userID` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `nonRecurringEvents`
--
ALTER TABLE `nonRecurringEvents`
  MODIFY `nrEventID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `recurringEvents`
--
ALTER TABLE `recurringEvents`
  MODIFY `rEventID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
