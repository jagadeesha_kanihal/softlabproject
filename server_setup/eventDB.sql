-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 20, 2016 at 11:24 PM
-- Server version: 5.7.16-0ubuntu0.16.04.1
-- PHP Version: 7.0.8-0ubuntu0.16.04.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eventDB`
--

-- --------------------------------------------------------

--
-- Table structure for table `credentialsTable`
--

CREATE TABLE `credentialsTable` (
  `userID` int(6) NOT NULL,
  `cseMail` varchar(100) NOT NULL,
  `password` varchar(128) NOT NULL,
  `iitbMail` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `credentialsTable`
--

INSERT INTO `credentialsTable` (`userID`, `cseMail`, `password`, `iitbMail`) VALUES
(1, 'dummy@cse.iitb.ac.in', 'dummy', 'dummy@iitb.ac.in'),
(2, 'sample@cse.iitb.ac.in', 'sample', 'sample.iitb.ac.in'),
(3, 'test@cse.iitb.ac.in', 'test', NULL),
(4, 'admin@cse.iitb.ac.in', 'admin', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `Days`
--

CREATE TABLE `Days` (
  `eDay` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Days`
--

INSERT INTO `Days` (`eDay`) VALUES
('Friday'),
('Monday'),
('Saturday'),
('Sunday'),
('Thursday'),
('Tuesday'),
('Wednesday');

-- --------------------------------------------------------

--
-- Table structure for table `groupMeta`
--

CREATE TABLE `groupMeta` (
  `groupID` varchar(256) NOT NULL,
  `groupMeta` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `groupMeta`
--

INSERT INTO `groupMeta` (`groupID`, `groupMeta`) VALUES
('CS601-Algo', 'Prof.Sundar V'),
('CS699-Software Lab', 'Prof. RK Joshi'),
('Mtech1', 'All Mtech 1 Students');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `groupID` varchar(256) NOT NULL,
  `userID` int(11) NOT NULL,
  `isAdmin` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`groupID`, `userID`, `isAdmin`) VALUES
('CS601-Algo', 1, 1),
('CS601-Algo', 2, NULL),
('CS699-Software Lab', 1, 1),
('CS699-Software Lab', 2, NULL),
('Mtech1', 1, 1),
('Mtech1', 2, 1),
('Mtech1', 3, NULL),
('Mtech1', 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `nonRecurringEvents`
--

CREATE TABLE `nonRecurringEvents` (
  `nrEventID` int(11) NOT NULL,
  `eDate` date NOT NULL,
  `startTime` time NOT NULL,
  `endTime` time NOT NULL,
  `eventName` varchar(256) NOT NULL,
  `eventPerson` varchar(128) DEFAULT NULL,
  `eventDescription` text,
  `groupID` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nonRecurringEvents`
--

INSERT INTO `nonRecurringEvents` (`nrEventID`, `eDate`, `startTime`, `endTime`, `eventName`, `eventPerson`, `eventDescription`, `groupID`) VALUES
(1, '2016-11-09', '17:30:00', '18:30:00', 'FUSS Talk', 'Prof.XYZ', 'Talk is about so and so topic.', 'Mtech1'),
(2, '2016-11-02', '17:30:00', '18:30:00', 'FUSS Talk', 'Prof.ABC', 'Talk is about so and so topic.', 'CS699-Software Lab'),
(3, '2016-10-30', '13:40:00', '23:59:00', 'FUSS Talk', 'Prof.ABC', 'Talk is about so and so topic @ so and so place.', 'CS699-Software Lab'),
(4, '2016-10-30', '10:55:00', '23:59:00', 'FUSS Talk', 'Prof.ABC', 'Talk is about so and so topic @ so and so place.', 'CS699-Software Lab'),
(5, '2016-10-31', '11:32:00', '23:59:00', 'FUSS Talk', 'Prof.ABC', 'Talk is about so and so topic @ so and so place.', 'CS699-Software Lab'),
(6, '2016-10-31', '16:37:00', '23:59:00', 'FUSS Talk', 'Prof.ABC', 'Talk is about so and so topic @ so and so place.', 'CS699-Software Lab'),
(7, '2016-10-31', '17:38:00', '23:59:00', 'FUSS Talk', 'Prof.ABC', 'Talk is about so and so topic @ so and so place.', 'CS699-Software Lab'),
(8, '2016-10-31', '16:35:41', '23:59:00', 'FUSS Talk', 'Prof.ABC', 'Talk is about so and so topic @ so and so place.', 'CS699-Software Lab'),
(9, '2016-10-31', '16:34:41', '23:59:00', 'FUSS Talk', 'Prof.ABC', 'Talk is about so and so topic @ so and so place.', 'CS699-Software Lab'),
(10, '2016-11-09', '19:50:41', '14:20:00', 'FUSS Talk', 'Prof.ABC', '7:50 PM non rec event 9th nov', 'CS699-Software Lab'),
(11, '2016-11-09', '17:30:00', '18:30:00', 'FUSS Talk', 'Prof.XYZ', 'Talk is about so and so topic.', 'Mtech1'),
(12, '2016-11-09', '17:30:00', '18:30:00', 'Talk', 'Prof.XYZ', 'Talk is about so and so topic.', 'Mtech1'),
(13, '2016-10-22', '16:00:00', '17:51:00', 'cs699demo', 'prashanth', 'soft lab project', 'CS601-Algo'),
(14, '2016-10-08', '16:53:00', '17:53:00', 'softlab', 'prashanth', 'demo', 'CS601-Algo'),
(15, '2016-11-08', '18:00:00', '19:00:00', 'soft lab dem', 'Jagadeesha', 'soft lab dem\nJagadeesha', 'CS601-Algo'),
(16, '2016-11-09', '23:35:00', '23:40:00', 'softdemo', 'Prashanth', 'softdemo\nPrashanth', 'Mtech1'),
(17, '2016-11-09', '23:38:00', '23:50:00', 'gdhd', 'shvd', 'gabc', 'Mtech1');

-- --------------------------------------------------------

--
-- Table structure for table `recurringEvents`
--

CREATE TABLE `recurringEvents` (
  `rEventID` int(11) NOT NULL,
  `eDay` varchar(30) NOT NULL,
  `startTime` time NOT NULL,
  `endTime` time NOT NULL,
  `slotNo` varchar(30) DEFAULT NULL,
  `eventName` varchar(256) NOT NULL,
  `eventPerson` varchar(128) DEFAULT NULL,
  `eventDescription` text,
  `groupID` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recurringEvents`
--

INSERT INTO `recurringEvents` (`rEventID`, `eDay`, `startTime`, `endTime`, `slotNo`, `eventName`, `eventPerson`, `eventDescription`, `groupID`) VALUES
(1, 'Monday', '19:02:00', '12:30:00', '4A', 'CS699-Lab Lecture', 'Prof.RKJ', '', 'CS699-Software Lab'),
(2, 'Wednesday', '19:00:03', '17:00:00', 'LX', 'CS699-Lab', 'Prof.RKJ', 'Software Lab ', 'CS699-Software Lab'),
(3, 'Tuesday', '10:30:00', '11:30:00', '4A', 'CS601-Lecture', 'Prof.SV', NULL, 'CS601-Algo'),
(4, 'Friday', '11:30:00', '12:30:00', '4A', 'CS699-Lab Lecture', 'Prof.RKJ', NULL, 'CS699-Software Lab'),
(5, 'Thursday', '13:30:00', '14:30:00', '4b', 'CS601-Lecture', 'Prof.SV', 'CS601-Lecture\nProf.SV\n4A', 'CS601-Algo'),
(6, 'Saturday', '14:00:00', '17:00:00', 'LX', 'CS699-Lab', 'Prof.RKJ', NULL, 'CS699-Software Lab'),
(7, 'Sunday', '10:30:00', '11:30:00', '4A', 'CS601-Lecture', 'Prof.SV', NULL, 'CS601-Algo'),
(8, 'Sunday', '08:38:00', '14:30:00', '4A', 'CS601-Lecture', 'Prof.SV', 'CS601-Lecture\nProf.SV\n4A', 'CS601-Algo'),
(9, 'Monday', '10:35:00', '12:13:00', '4A', 'CS699-Lab Lecture', 'Prof.RKJ', '', 'CS699-Software Lab'),
(10, 'Monday', '15:35:00', '16:59:00', '4A', 'CS601-Lecture', 'Prof.SV', 'CS601-Lecture\nProf.SV\n4A', 'Mtech1'),
(11, 'Monday', '20:54:00', '12:13:00', '4A', 'CS699-Lab Lecture', 'Prof.RKJ', '', 'CS699-Software Lab'),
(12, 'Thursday', '10:30:00', '11:30:00', '4A', 'CS601-Lecture', 'Prof.SVhdhs', 'CS601-Lecture\nProf.SV\n4A', 'CS601-Algo'),
(13, 'Monday', '15:02:00', '12:30:00', '4A', 'CS699-Lab Lecture', 'Prof.RKJ', '', 'Mtech1'),
(14, 'Sunday', '08:38:00', '17:30:00', '4A', 'CS601-Lecture', 'Prof.SV', 'Update test CS601-Lecture\nProf.SV\n4A', 'CS601-Algo'),
(15, 'Wednesday', '10:30:00', '11:30:00', '4A', 'CS601-Lecture', 'Prof.SV', '', 'CS601-Algo'),
(16, 'Wednesday', '19:47:03', '21:01:00', 'LX', 'CS699-Lab', 'Prof.RKJ', '7:47PM notification- Wednesday', 'CS699-Software Lab');

-- --------------------------------------------------------

--
-- Table structure for table `regIdTable`
--

CREATE TABLE `regIdTable` (
  `userID` int(11) NOT NULL,
  `regID` varchar(256) NOT NULL,
  `deviceID` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `regIdTable`
--

INSERT INTO `regIdTable` (`userID`, `regID`, `deviceID`) VALUES
(1, 'dbV5P2fnPVI:APA91bE3i55scxqhfJem6vYO9Gac0WQgGRLUwNlQ2t2h8x4VQYLC7uXJAraF5_RBq_5akGP4IYJc_g8Jo-imXeKpuGJM43O9aGvB1Jd0C064pKmH3PfwnRByyy7vJSnNrApql3855XGY', NULL),
(2, 'cYCZq2RPMZo:APA91bErVDhIjiN9E8chEr6ZnW7TmsV_Ozxltkm2ILatyOOxwhrQazl3J_7TCuLZOD5k7ssUgyLJheOqfCg5CEApxRkHJEorzpwAymOhQXfJ7-ZtqlZoT1AIKv9gN31EXNPpHGbTGWFt', NULL),
(3, 'jkl12345', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `credentialsTable`
--
ALTER TABLE `credentialsTable`
  ADD PRIMARY KEY (`userID`),
  ADD UNIQUE KEY `userID` (`userID`),
  ADD UNIQUE KEY `cseMail` (`cseMail`);

--
-- Indexes for table `Days`
--
ALTER TABLE `Days`
  ADD PRIMARY KEY (`eDay`);

--
-- Indexes for table `groupMeta`
--
ALTER TABLE `groupMeta`
  ADD PRIMARY KEY (`groupID`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`groupID`,`userID`);

--
-- Indexes for table `nonRecurringEvents`
--
ALTER TABLE `nonRecurringEvents`
  ADD PRIMARY KEY (`nrEventID`);

--
-- Indexes for table `recurringEvents`
--
ALTER TABLE `recurringEvents`
  ADD PRIMARY KEY (`rEventID`),
  ADD UNIQUE KEY `eventID` (`rEventID`);

--
-- Indexes for table `regIdTable`
--
ALTER TABLE `regIdTable`
  ADD PRIMARY KEY (`userID`,`regID`),
  ADD KEY `userID` (`userID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `credentialsTable`
--
ALTER TABLE `credentialsTable`
  MODIFY `userID` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `nonRecurringEvents`
--
ALTER TABLE `nonRecurringEvents`
  MODIFY `nrEventID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `recurringEvents`
--
ALTER TABLE `recurringEvents`
  MODIFY `rEventID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
