#!/bin/bash

sudo apt-get install php-xdebug 
#xdebug configuaration - reference
#https://laracasts.com/discuss/channels/laravel/how-to-set-up-phpstormlatest-homestead-php7-xdebug?page=1
#http://acacha.org/mediawiki/Homestead#Xdebug
#----------put this in /etc/php/7.0/mods-available/xdebug.ini or it's symbolic link /etc/php/7.0/fpm/conf.d/20-xdebug.ini
# zend_extension=/usr/lib/php/20151012/xdebug.so
# xdebug.idekey=PHPSTORM
# xdebug.remote_enable = 1
# xdebug.remote_connect_back = 1
# xdebug.remote_port = 9000

# xdebug.max_nesting_level = 400
# xdebug.scream = 0
# xdebug.cli_color = 1
# xdebug.show_local_vars = 1


cd /etc/php/7.0/fpm/conf.d/

#sudo service php7.0-fpm restart

php -v

sudo sed  -i "|zend_extension=xdebug.so| { s|zend_extension=xdebug.so|zend_extension=/usr/lib/php/20151012/xdebug.so| }" /etc/php/7.0/mods-available/xdebug.ini

echo 'xdebug.remote_port=9000'      >> /etc/php/7.0/mods-available/xdebug.ini
echo 'xdebug.remote_mode=req'       >> /etc/php/7.0/mods-available/xdebug.ini
#echo 'xdebug.remote_host=192.168.11.11' >> /etc/php/7.0/mods-available/xdebug.ini
echo 'xdebug.remote_handler=dbgp'   >> /etc/php/7.0/mods-available/xdebug.ini
echo 'xdebug.remote_connect_back=1' >> /etc/php/7.0/mods-available/xdebug.ini
echo 'xdebug.remote_enable=1'       >> /etc/php/7.0/mods-available/xdebug.ini
#echo 'xdebug.remote_autostart=0'    >> /etc/php/7.0/mods-available/xdebug.ini
echo 'xdebug.max_nesting_level=400' >> /etc/php/7.0/mods-available/xdebug.ini
echo 'xdebug.idekey=PHPSTORM'       >> /etc/php/7.0/mods-available/xdebug.ini
echo 'xdebug.default_enable=1'      >> /etc/php/7.0/mods-available/xdebug.ini
echo 'xdebug.cli_color=1'           >> /etc/php/7.0/mods-available/xdebug.ini
echo 'xdebug.scream=0'              >> /etc/php/7.0/mods-available/xdebug.ini
echo 'xdebug.show_local_vars=1'     >> /etc/php/7.0/mods-available/xdebug.ini

ln -s /etc/php/7.0/mods-available/xdebug.ini /etc/php/7.0/cli/conf.d/20-xdebug.ini

sudo service php7.0-fpm restart

php -v
