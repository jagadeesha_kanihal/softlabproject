<?php
header('Content-Type: application/json');

require_once __DIR__ . '/dbConfig.php';
// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}


if (isset($_POST["userID"])) {
    $userID=$_POST["userID"];
}
$grows = array();
$sql = "SELECT * FROM groups WHERE userID='$userID'";
$result = $conn->query($sql);
if ($result->num_rows > 0) {

    while($row = $result->fetch_assoc()) {
        $grows[]=$row["groupID"];
    }
    //echo json_encode($grows);
}

$first=0;
foreach ($grows as $s){
    if ($first==0){
        $str='\''.$s.'\'';
        $first=1;
    }
    else {
        $str = $str . "," . '\'' . $s . '\'';
    }
}

$sql = "SELECT * FROM nonRecurringEvents WHERE groupID IN ($str)";
$result = $conn->query($sql);
    if ($result->num_rows > 0) {
        $rows = array();
        while($row = $result->fetch_assoc()) {
            $rows[]=$row;
        }
        echo json_encode($rows);
    }
else{
    // echo $conn->error;
}
$conn->close();
?>