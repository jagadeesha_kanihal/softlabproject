<!DOCTYPE HTML>
<html>
<head>

    <!-- Change author to your name -->
    <meta name="author" content="Prashanth"/>

    <!-- Title of Page -->
    <title>
        login test
    </title>

    <!-- Bootstrap -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- <script src="../bootstrap/js/bootstrap.min.js"></script> -->

    <script>
        function showWarning(str) {
            elem=document.getElementById("warn");
            elem.innerHTML=str;
        }
    </script>

</head>

<body>

<!-- Page Container -->
<div class="container">
    <p id="warn" style="color: orange;margin-left: 120px" ></p>
    <div class="row" style="min-height:500px; width: 700px" >
        <div class="col-xs-12">

            <form action="insertRecEvent.php" method="post" class="form-horizontal">
                <div class="form-group">
                    <label class="control-label col-sm-2" for="User">rEvent:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="rEvent" placeholder="" required>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2" for="pwd"></label>
                    <div class="col-sm-10">
                        <input type="submit" name="submit" class="btn btn-primary" value="submit">
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
</body>
</html>
