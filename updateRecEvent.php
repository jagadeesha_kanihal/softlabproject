<?php
require_once __DIR__ . '/dbConfig.php';
// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

if (isset($_POST["rEvent"])) {
    $rEventJSON=$_POST["rEvent"];
    
    #decode json and convert to associative array
    $rEventArr=json_decode($rEventJSON,true);

    $rEventID=$rEventArr["rEventID"];
    
    $eDay=$rEventArr["eDay"];
    $startTime=$rEventArr["startTime"];
    $endTime=$rEventArr["endTime"];
    $slotNo=$rEventArr["slotNo"];
    $eventName=$rEventArr["eventName"];
    $eventPerson=$rEventArr["eventPerson"];
    $eventDescription=$rEventArr["eventDescription"];
    $groupID=$rEventArr["groupID"];


    $sqlForUpdate = "UPDATE recurringEvents SET eDay='$eDay', startTime='$startTime', endTime='$endTime', slotNo='$slotNo', eventName='$eventName', eventPerson='$eventPerson', eventDescription='$eventDescription', groupID='$groupID' WHERE rEventID='$rEventID'";
    $uresult = $conn->query($sqlForUpdate);
    if ($uresult === TRUE){
      echo "rEvent updated successfully";
  }
  else{
    echo "Error updating rEvent: " . $conn->error;
}
}
$conn->close();
?>