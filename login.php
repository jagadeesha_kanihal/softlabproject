<?php
$debug = 0;
header('Content-Type: application/json');
function updateRegID($dbuserID, $uRegID)
{
    global $conn, $debug;
    $sql    = "SELECT * FROM regIdTable WHERE userID='$dbuserID'";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        if ($row = $result->fetch_assoc()) {
            $dbRegID = $row["regID"];
        }
        
        if ($dbRegID != $uRegID) {
            $sqlForUpdate = "UPDATE regIdTable SET regID='$uRegID' WHERE userID='$dbuserID'";
            $uresult      = $conn->query($sqlForUpdate);
            if ($uresult === TRUE) {
                if ($debug) {
                    echo "regID updated successfully";
                }
            }
            
            else {
                if ($debug) {
                    echo "Error updating regID: " . $conn->error;
                }
            }
        }
    } else {
        $usql    = "INSERT INTO regIdTable (userID,regID) VALUES ('$dbuserID','$uRegID')";
        $uresult = $conn->query($usql);
        if ($uresult === TRUE) {
            // if (($conn->affected_rows) == -1){
            if ($debug) {
                echo "regID inserted successfully ";
            }
            // }
            // else{
            //       #this case is not possible
            //       #echo "Event with $eventID may already exist in the DB, check and try again";
            // }
        } else {
            if ($debug) {
                echo "Error updating regID: " . $conn->error;
            }
        }
    }
}

require_once __DIR__ . '/dbConfig.php';
// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

if (isset($_POST["cseMail"]) and isset($_POST["password"])) {
    $uCseMail  = $_POST["cseMail"];
    $uPassword = $_POST["password"];
    if (isset($_POST["regID"])) {
        $uRegID = $_POST["regID"];
    }
}


$sql    = "SELECT * FROM credentialsTable WHERE cseMail='$uCseMail'";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    //valid username
    if ($row = $result->fetch_assoc()) {
        $dbCseMail  = $row["cseMail"];
        $dbPassword = $row["password"];
        $dbuserID   = $row["userID"];
        
    }
    if ($uCseMail == $dbCseMail and $uPassword == $dbPassword) {
        $stat           = array();
        $stat["status"] = "SUCCESS";
        $stat["userID"] = $dbuserID;
        
        //update regid corresponding to the user id on first login
        //check if the sent reg id same as what is there in the db, if not update
        if ($uRegID) {
            updateRegID($dbuserID, $uRegID);
        }
        echo json_encode($stat);
    } else {
        if ($debug) {
            echo "invalid password";
        }
    }
} else {
    if ($debug) {
        echo "invalid username";
    }
}


$conn->close();
?>