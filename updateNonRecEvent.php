<?php
require_once __DIR__ . '/dbConfig.php';
// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}


if (isset($_POST["nrEvent"])) {
    $nrEventJSON=$_POST["nrEvent"];

    #decode json and convert to associative array
    $nrEventArr=json_decode($nrEventJSON,true);

    $nrEventID=$nrEventArr["nrEventID"];

    $eDate=$nrEventArr["eDate"];
    $startTime=$nrEventArr["startTime"];
    $endTime=$nrEventArr["endTime"];
    $eventName=$nrEventArr["eventName"];
    $eventPerson=$nrEventArr["eventPerson"];
    $eventDescription=$nrEventArr["eventDescription"];
    $groupID=$nrEventArr["groupID"];



    $sqlForUpdate = "UPDATE nonRecurringEvents SET eDate='$eDate', startTime='$startTime', endTime='$endTime', eventName='$eventName', eventPerson='$eventPerson', eventDescription='$eventDescription', groupID='$groupID' WHERE nrEventID='$nrEventID'";
    $uresult = $conn->query($sqlForUpdate);
    if ($uresult === TRUE){
      echo "nrEvent updated successfully";
  }
  else{
    echo "Error updating nrEvent: " . $conn->error;
}
}
$conn->close();

?>