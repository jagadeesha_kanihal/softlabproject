<?php
header('Content-Type: application/json');

require_once __DIR__ . '/dbConfig.php';
// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

if (isset($_POST["userID"])) {
       $userID=$_POST["userID"];
 }
#AND (isAdmin IS NOT NULL)
$sql = "SELECT groupID FROM groups WHERE userID='$userID' AND isAdmin=1";

$grows=array();
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    $grows = array();
    while($row = $result->fetch_assoc()) {
        $grows[]=$row["groupID"];
    }
    //echo json_encode($rows);
}


$first=0;
foreach ($grows as $s){
    if ($first==0){
        $str='\''.$s.'\'';
        $first=1;
    }
    else {
        $str = $str . "," . '\'' . $s . '\'';
    }
}


$sql = "SELECT * FROM groupMeta WHERE groupID IN ($str)";


$grows=array();
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    $grows = array();
    while($row = $result->fetch_assoc()) {
        $grows[]=$row;
    }
}
echo json_encode($grows);

$conn->close();
?>