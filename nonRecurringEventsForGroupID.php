<?php
header('Content-Type: application/json');
require_once __DIR__ . '/dbConfig.php';
// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}


if (isset($_POST["groupID"])) {
    $groupID=$_POST["groupID"];
}

$sql = "SELECT * FROM nonRecurringEvents WHERE groupID='$groupID'";
$result = $conn->query($sql);
$rows = array();
if ($result->num_rows > 0) {

    while($row = $result->fetch_assoc()) {
        $rows[]=$row;
    }
}
else{
    // echo $conn->error;
}
echo json_encode($rows);
$conn->close();
?>