<?php
require_once __DIR__ . '/dbConfig.php';
require_once __DIR__ . '/firebase.php';

$debug = 0;

function findEventsAndSend()
{
    global $servername, $username, $password, $dbname;
    global $firebase;
    global $debug;
    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    
    
    $date1 = date("Y-m-d", strtotime("+14 minutes"));
    $date2 = date("Y-m-d", strtotime("+15 minutes"));
    
    $time1 = date("H:i:s", strtotime("+14 minutes"));
    $time2 = date("H:i:s", strtotime("+15 minutes"));
    
    //for testing purpose
    if ($debug) {
        $time1 = "19:00:00";
    }
    $day1 = date("l", strtotime("+14 minutes"));
    $day2 = date("l", strtotime("+15 minutes"));
    
    // echo $date1. "\n";
    // echo $date2. "\n";
    // echo $time1. "\n";
    // echo $time2. "\n";
    // echo $day1;
    $rows   = array();
    $msg    = array();
    $regIDs = array();
    
    
    
    $rSql  = "SELECT * FROM recurringEvents WHERE eDay='$day1' AND (startTime BETWEEN '$time1' AND '$time2')";
    $nrSql = "SELECT * FROM nonRecurringEvents WHERE eDate='$date1' AND (startTime BETWEEN '$time1' AND '$time2')";
    
    #get the events
    $result = $conn->query($rSql);
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $rows[] = $row;
        }
    }
    
    #get userids -> reg ids and send push notification
    for ($i = 0; $i < sizeof($rows); $i++) {
        $groupID = $rows[$i]["groupID"];
        
        $msg['title'] = $rows[$i]["eventName"];
        $msg['body']  = $rows[$i]["eventDescription"] . ":by " . $rows[$i]["eventPerson"] . " - starts in 15 minutes";
        // $push['notification']=$msg;
        
        $sql    = "SELECT userID FROM groups WHERE groupID='$groupID'";
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
            unset($regIDs);
            $regIDs = array();
            while ($row = $result->fetch_assoc()) {
                $userID = $row["userID"];
                $rID    = getRegId($userID);
                if ($rID != null) {
                    $regIDs[] = $rID;
                }
            }
            // $push['to']=$regIDs;
        }
        echo "sending notification\n";
        if (sizeof($regIDs) <= 20) {
            $firebase->sendNotification($regIDs, $msg);
        } else {
            $chunkedRegIds = array_chunk($regIDs, 20);
            for ($i = 0; $i < sizeof($chunkedRegIds); $i++) {
                $firebase->sendNotification($chunkedRegIds[$i], $msg);
            }
        }
        
    }
    
    
    #repeat for non recurring events
    #get the events
    unset($rows);
    $rows = array();
    $result = $conn->query($nrSql);
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $rows[] = $row;
        }
    }
    
    #get userids -> reg ids and send push notification
    for ($i = 0; $i < sizeof($rows); $i++) {
        $groupID = $rows[$i]["groupID"];
        
        $msg['title'] = $rows[$i]["eventName"];
        $msg['body']  = $rows[$i]["eventDescription"] . "-by " . $rows[$i]["eventPerson"] . " - starts in 15 minutes";
        // $push['notification']=$msg;
        
        $sql    = "SELECT userID FROM groups WHERE groupID='$groupID'";
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
            unset($regIDs);
            $regIDs = array();
            while ($row = $result->fetch_assoc()) {
                $userID = $row["userID"];
                $rID    = getRegId($userID);
                if ($rID != null) {
                    $regIDs[] = $rID;
                }
            }
            // $push['to']=$regIDs;
        }
        echo "sending notification\n";
        if (sizeof($regIDs) <= 20) {
            $firebase->sendNotification($regIDs, $msg);
        } else {
            $chunkedRegIds = array_chunk($regIDs, 20);
            for ($i = 0; $i < sizeof($chunkedRegIds); $i++) {
                $firebase->sendNotification($chunkedRegIds[$i], $msg);
            }
        }
        
    }
    $conn->close();
}

function getRegId($userID)
{
    global $servername, $username, $password, $dbname;
    $conn2 = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn2->connect_error) {
        die("Connection failed: " . $conn2->connect_error);
    }
    
    $sql    = "SELECT regID FROM regIdTable WHERE userID='$userID'";
    $result = $conn2->query($sql);
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $regID = $row["regID"];
        }
    }
    $conn2->close();
    return ($regID);
}

//FORMAT of Notification
// { "notification": {
//     "title": "Portugal vs. Denmark",
//     "body": "5 to 1"
//   },
//   "to" : "bk3RNwTe3H0:CI2k_HHwgIpoDKCIZvvDMExUdFQ3P1..."
// }


// $msg['is_background'] = "is_background";
// $msg['message'] = "message";
// $msg['image'] = "image";
// $msg['payload'] = "data";
// $msg['timestamp'] = date('Y-m-d G:i:s');

// $push['data']=$msg;
// $push['to']=$regIDs;
$firebase = new Firebase();

//can be done using either sleep : (might cause memory issue)
//cronjob(i prefer this):    * * * * * /usr/bin/php /var/www/html/softLabProject/push.php
//nohup(no hangup) & puts it in the background, use kill pid to kill it: nohup php myscript.php &

// while (1) {
//     findEventsAndSend();
//     //sleep(60);
// }
echo "\n ---checking for events...\n";
findEventsAndSend();
?>