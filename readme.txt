Project: IITB-Schedule
project description: classes, timetable, notifications - this android app will keep you posted all the time implemented using LAMP and Android

Team:
Jagadeesha - 163059002
Prashanth M- 163050043
####################################################################################################################
FileIndex :
Server: softLabProject folder
------------------------------
dbConfig.php                     - mysql db connection credentials
firebase.php                     - google firebase cloud messaging related api
getGroupsForUserID.php           - get groups for faculty
index.php                        - test index file
insertNonRecEvent.php            - insert event based on date
insertRecEvent.php               - insert event based on week day
localhost.sql                    - db file to populate new db 
login.php                        - for login checking
nonRecurringEventsForGroupID.php - get event based date for group id
nonRecurringEventsForUserID.php  - get event based date for user id
push.php                         - push notification to clients using google fcm
recurringEventsForGroupID.php    - get event based weekday for group id
recurringEventsForUserID.php     - get event based weekday for group id
testGroupsForUserID.php          - test file for groups
testLogin.php                    - test file for Login
testNREventsForGroupID.php       - test file for non recurring events for group
testNREventsForUserID.php        - test file for non recurring events for user
testNRInsert.php                 - test file for non rec event insert
testNRUpdate.php                 - test file for non rec event update
testREventsForGroupID.php        - test file for rec event for group
testREventsForUserID.php         - test file for rec event for user
testRInsert.php                  - test file for rec event insert
testRUpdate.php                  - test file for rec event update
updateNonRecEvent.php            - update non recurring event
updateRecEvent.php               - update recurring event

Client: softLabProject
----------------------



####################################################################################################################
PHP Server Setup
----------------
1.To install LAMP server required library modules run script @ server_setup/LAMP-setup.sh with root  previlages.

2.Change Bind address of mysql server and set access access previlages to mysql databases using instruction given @ server_setup/mysql-conf.txt

3.Import databases onto mysql server by loading localhost.sql or eventDB.sql using phpmyadmin portal

4.Change the credentials for mysql server database connection to credentials the above mentioned mysql server @ softLabProject/dbConf.php

5.Copy softLabProject folder to /var/www/html/

6.Create cronjob for sending notification by running script @ server_setup/cronjob-setup.sh

7.Create a cloud messaging service using google firebase(fcm), get server api key for the app and put in 'softLabProject/firebase.php'
    define('FIREBASE_API_KEY', 'AIzaSyABkjOkXZRtW5vlV99XMAWUEQqnySeCMAk'); 

How to use server to manage events
-----------------------------------
1.Goto <url or ip>/phpmyadmin
2.Login using database credentials
3.Goto eventsDB : if you want to add events based on weekdays you can add events to 'recurringEvents' table->insert
                  if you want to add events based on date you can add events to 'NonRecurringEvents' table->insert
4.Same process as mentioned in step 3 for deleting and updating events
--------------------------------------------------------------------------------------------------------------------

How to use Android Client
-----------------------------------


####################################################################################################################